<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title> Page de transition </title>
  <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
  <div>
    <br></br>
    <h1>Formulaire d'ajout d'un nouveau produit</h1>
    <br></br>
    <form action="liste_produit.php">
      <p>Merci d'avoir remplis ce formulaire</p>
      <p>Votre produit a bien été enregistré</p>
      <input type="submit" value="Liste des produits" >
    </form>
    <br></br>
    <form action="index.php">
      <p>Vous voulez ajouter un nouveau produit</p>
      <input type="submit" value="Enregistrer un nouveau produit"><br></br>
      <br></br>
    </form>
  </div>
</body>
</html>
